public class Villain extends Character {

    private int clumsiness;

    public Villain() {

    }

    public Villain(String name, int health, int damage, int gold, int clumsiness) {
        super(name, health, damage, gold);
        this.clumsiness = clumsiness;
    }

    public int getClumsiness() {
        return clumsiness;
    }

    public void setClumsiness(int clumsiness) {
        this.clumsiness = clumsiness;
    }

    @Override
    public String toString() {
        return "Villain{" +  "Name='" + this.getName() + '\'' + "Health='" + this.getHealth() + '\'' +
                "Damage='" + this.getDamage() + '\'' + "Gold='" + this.getGold() + '\'' +
                "Clumsiness" + this.getClumsiness() +
                '}';
    }
}
