import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        Hero hero=new Hero();
        System.out.println("Hello World!");

        Scanner in = new Scanner(System.in);
        System.out.println("Selectati numele:");
        hero.setName(in.nextLine());

        System.out.println("Selectati punctele de viata:");
        hero.setHealth(Integer.parseInt(in.nextLine()));

        System.out.println("Selectati damage-ul:");
        hero.setDamage(Integer.parseInt(in.nextLine()));

        System.out.println("Selectati gold-ul:");
        hero.setGold(Integer.parseInt(in.nextLine()));

        System.out.println("Selectati culoarea pielii:");
        hero.setSkinColor(in.nextLine());

        System.out.println("Selectati culoarea ochilor:");
        hero.setEyesColor(in.nextLine());

        System.out.println("Selectati culoarea parului:");
        hero.setHairColor(in.nextLine());

        //cream arena
        Challenge ch=new Challenge(hero,in);
        ch.afisareInamici();
        //cream threadul de lupta
        Thread thread=new Thread(ch);
        thread.start();

    }
}
