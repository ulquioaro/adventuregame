public class Hero extends Character{

    private String skinColor;
    private String eyesColor;
    private String hairColor;

    public Hero() {
      super();
    }

    public Hero(String name, int health, int damage, int gold, String skinColor, String eyesColor, String hairColor) {
        super( name,health, damage, gold);
        this.skinColor = skinColor;
        this.eyesColor = eyesColor;
        this.hairColor = hairColor;
    }

    public String getSkinColor() {
        return skinColor;
    }

    public void setSkinColor(String skinColor) {
        this.skinColor = skinColor;
    }

    public String getEyesColor() {
        return eyesColor;
    }

    public void setEyesColor(String eyesColor) {
        this.eyesColor = eyesColor;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    @Override
    public String toString() {
        return "Hero{" +  "Name='" + this.getName() + '\'' + "Health='" + this.getHealth() + '\'' +
                "Damage='" + this.getDamage() + '\'' + "Gold='" + this.getGold() + '\'' +
                "SkinColor='" + this.getSkinColor() + '\'' +
                ", EyesColor='" + this.getEyesColor() + '\'' +
                ", HairColor='" + this.getHairColor() + '\'' +
                '}';
    }
}
