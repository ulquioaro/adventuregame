public class Ogre extends Villain {
    public Ogre(String name, int health, int damage, int gold, int clumsiness) {
        super(name, health, damage, gold, clumsiness);
    }
    @Override
    public String toString() {
        return "Ogre{" +  "Name='" + this.getName() + '\'' + "Health='" + this.getHealth() + '\'' +
                "Damage='" + this.getDamage() + '\'' + "Gold='" + this.getGold() + '\'' +
                "Clumsiness" + this.getClumsiness() +
                '}';
    }
}
