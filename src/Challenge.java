import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.*;

public class Challenge extends Thread {
    private Scanner in;
    private String actiune;
   private Hero hero;
   private List<Villain> villains=new ArrayList<Villain>(9);


    public Challenge(Hero hero,Scanner s) {
        this.in=s;
        this.actiune="";
        this.hero = hero;
       for(int i=0;i<10;i++){
           int j=getRandomBetween(0,3);
           switch (j){
               case 0:
                  Ogre ogre=new Ogre("Ogre "+i,getRandomBetween(150,201),getRandomBetween(30,51),
                          getRandomBetween(1,1001),getRandomBetween(60,101));
                   villains.add((Villain)ogre);
                   break;
               case 1:
                   Bandit bandit=new Bandit("Bandit "+i,getRandomBetween(1,201),getRandomBetween(1,51),
                           getRandomBetween(1,301),getRandomBetween(1,26));
                   villains.add((Villain)bandit);
                   break;
               case 2:
                   Champion champion=new Champion("Champion "+i,getRandomBetween(1,201),getRandomBetween(1,51),
                           getRandomBetween(750,1001),getRandomBetween(1,101));
                   villains.add((Villain)champion);
                   break;
           }
       }
    }
    //genereaza u nr intre in intervalul[low,hight)
    public int getRandomBetween(int low, int high) {
        Random r = new Random();
        int result = r.nextInt(high - low) + low;
        return result;
    }
    //realizeaza functia de sneaking
    public boolean sneak(int chance){
        int cSneaky=getRandomBetween(1,100);
        if(cSneaky<=chance){
            return true;
        }
        else
            return false;
    }
    //afiseaza detalii despre combatanti
    public void afisareInamici(){
        System.out.println("Detalii erou:");
        System.out.println(hero.toString());
        System.out.println('\'' + "Detalii raufacatori:");
        for(Villain v:villains)
            System.out.println(v.toString());
    }
    @Override
    public void run() {
        int index=0;
        int isFight=0;
        while(index<10 && hero.getHealth()>0){
            System.out.println(hero.toString());
            System.out.println("Adversarul tau este: "+villains.get(index).getClass()+ " Damage:"+villains.get(index).getDamage()+" Health:"
            +villains.get(index).getHealth()+" Spaga pe care o cere:"+villains.get(index).getGold() +" Sansa sa doarma:"
            +villains.get(index).getClumsiness());
            if(isFight==0) {//daca nu e lupta fortata atunci putem alege orice actiune vrem
                System.out.println("Alege actiunea:");
                actiune = in.nextLine();
            }
            else
                actiune="fight";
            switch (actiune){
                case "fight"://daca actiunea e fight fiecare pierde viaza proportional cu damage-ul celuilalt
                    while(hero.getHealth()>0 && villains.get(index).getHealth()>0){
                        try{
                        hero.setHealth(hero.getHealth()- villains.get(index).getDamage());
                        villains.get(index).setHealth(villains.get(index).getHealth()-hero.getDamage());
                        System.out.println("Status dupa strike: Hero: "+hero.getHealth() + "  "+villains.get(index).getName() +
                                ": "+villains.get(index).getHealth());

                           Thread.sleep(1000);
                        }
                        catch (InterruptedException e){
                            e.printStackTrace();
                        }
                    }
                    isFight=0;
                    index++;
                    break;
                case "bribe"://incearca sa mitueasca iar daca nu are suficient e pus sa aleaga o alta varianta
                    try {
                        if (hero.getGold() > villains.get(index).getGold()) {
                            hero.setGold((hero.getGold()-villains.get(index).getGold()));
                            System.out.println("Ai dat spaga sa treci!");
                            index++;
                        }
                        else
                            System.out.println("Nu ai cuficient cash! Incearca alta varianta.");
                        Thread.sleep(1000);
                    }
                    catch(InterruptedException e){
                        e.printStackTrace();
                    }
                    break;

                case "sneak"://daca nu reuseste sa se furiseze atunci va lupta fortat
                    try{
                        if(sneak(villains.get(index).getClumsiness())){
                            System.out.println("Te-ai strecurat cu succes!");
                            index++;
                        }
                        else {
                            isFight = 1;
                            villains.get(index).setHealth( villains.get(index).getHealth()*2);
                            villains.get(index).setDamage( villains.get(index).getDamage()*2);
                            System.out.println("Ai esuat , trebuie sa lupti!");
                        }
                        Thread.sleep(1000);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }
                    break;
            }
        }
       if(hero.getHealth()>0){
           System.out.println("Ai castigat!");
       }
       else
           System.out.println("Ai pierdut!");

    }
}
