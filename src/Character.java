public class Character {
    private String name;
    private int health;
    private int damage;
    private int gold;

    public Character() {

    }

    public Character(String name, int health, int damage, int gold) {
        this.name = name;
        this.health = health;
        this.damage = damage;
        this.gold = gold;
    }
    public Character( int health, int damage, int gold) {
        this.name = name;
        this.health = health;
        this.damage = damage;
        this.gold = gold;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }
}
